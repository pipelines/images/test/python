Python test container image
===

Container image with *Tox* and a Python dev environment, based
on Debian stable.

You can run a basic Tox test by including the [ci.yml](ci.yml)
file from this repository in your CI script. The script supports
defining the `PY_TEST_PACKAGES` environment variable to install
Debian packages if needed.

tox integration
---
To get started, a `tox.ini` file looks like this:

```ini
[tox]
envlist = py3

[testenv]
deps=
  pytest
  pytest-cov
commands=
  pytest --cov --cov-report term --cov-report xml:cover.xml --junitxml pytest.xml []
```

Make sure to pass your module name to `--cov` for properly-scoped coverage results.
