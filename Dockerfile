FROM docker.io/library/debian:stable-slim

RUN apt-get -q update && \
    env DEBIAN_FRONTEND=noninteractive apt-get -q -y install --no-install-recommends \
        build-essential python3 python3-dev python3-pip tox git brotli && \
    apt-get clean && \
    rm -fr /var/lib/apt/lists/*
